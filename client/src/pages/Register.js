import './assets/css/Auth.css'
import { useState  } from 'react';
import { Card, Form, Button, Row } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Authenticated from '../hoc/Authenticated';

function Register(){
    const [validated, setValidated] = useState(false);
    const [username, setUsername] = useState(null);
    const [fullname, setFullname] = useState(null);
    const [password, setPassword] = useState(null);
    const [email, setEmail] = useState(null);

    const navigate = useNavigate();

    function createNewUser(username, email, password){
        localStorage.setItem("isAuthenticated", JSON.stringify(true));
        localStorage.setItem("user", JSON.stringify({
            username,
            fullname,
            email,
            level: "Novice",
            point: 0
        }))
        // Redirect to home
        navigate("/");
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }else{
            setValidated(true);
            createNewUser(username, fullname, email, password);
        }

        
    };

    return(
        <Authenticated>
        <div className="vw-100 vh-100 Auth-background container-fluid">
            <div className="row justify-content-center my-auto h-100 align-items-center d-flex">
                <div className="col-md-5">
                    <Card className="p-4">
                        <Link to={"/"}><i className="bi bi-arrow-left-square-fill fs-1 p-2"></i></Link>
                        <Card.Body className="align-items-center d-flex flex-column">
                            <Card.Title className="fs-1 mb-5 fw-bold">REGISTER</Card.Title>
                            
                            <Form noValidate validated={validated} onSubmit={handleSubmit} className="w-100">
                                <Row className="mb-3">
                                    <Form.Group controlId="validationCustom01">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            placeholder="Username"
                                            pattern="^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
                                            onChange={(e) => {setUsername(e.target.value)}}
                                        />
                                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">Username must be an alphanumeric, with length minimum 6 and maximum 20</Form.Control.Feedback>
                                    </Form.Group>
                                </Row>
                                <Row className="mb-3">
                                    <Form.Group controlId="validationCustom02">
                                        <Form.Label>Fullname</Form.Label>
                                        <Form.Control 
                                            type="text" 
                                            placeholder="Fullname" 
                                            onChange={(e) => {setFullname(e.target.value)}}
                                            required />
                                        <Form.Control.Feedback type="invalid">
                                            Please provide a valid email
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Row>
                                <Row className="mb-3">
                                    <Form.Group controlId="validationCustom03">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control 
                                            type="email" 
                                            placeholder="Email" 
                                            onChange={(e) => {setEmail(e.target.value)}}
                                            required />
                                        <Form.Control.Feedback type="invalid">
                                            Please provide a valid email
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Row>
                                <Row className="mb-3">
                                <Form.Group controlId="validationCustom04">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control 
                                            type="text" 
                                            placeholder="Password" 
                                            onChange={(e) => {setPassword(e.target.value)}}
                                            required 
                                            pattern='^\S{6,}$'/>
                                        <Form.Control.Feedback type="invalid">
                                            Please provide a valid password with minimum length of 6
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Row>
                                    <Button type="submit" variant="warning" className="text-white fw-bold fs-5 w-100" size="sm">Register</Button>
                                </Form>

                            <Card.Subtitle className="my-1 text-muted">
                                Already have an account? <Link to={"/login"}>Login</Link>
                            </Card.Subtitle>
                            
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </div>
        </Authenticated>
    )
}

export default Register;
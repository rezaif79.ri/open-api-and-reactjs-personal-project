import './assets/css/Lobby.css'
import { Row, Col, ToggleButtonGroup, ToggleButton } from "react-bootstrap";
import PlayerHeader from '../components/PlayerHeader';
import { useState } from 'react';
import avatar from './assets/img/ava3.jpg';
import users from './assets/user.json';
import PlayerCardLobby from '../components/PlayerCardLobby';
import Footer from '../components/Footer';

function PlayerLobby(){
    const isAuthenticated = localStorage.getItem('isAuthenticated');
    const getUser = localStorage.getItem('user');
    const user = JSON.parse(getUser);

    const [categoryValue, setCategoryValue] = useState('ALL');
    const category = [
        { name: 'ALL', value: "all"}
        ,{ name: 'Novice', value: "novice"}
        ,{ name: 'Class A', value: "class-a"}
        ,{ name: 'Class B', value: "class-b"}
        ,{ name: 'Class C', value: "class-c"}
        ,{ name: 'Class D', value: "class-d"}
        ,{ name: 'Candidate Master', value: "candidate-master"}
        ,{ name: 'Grand Master', value: "grand-master"}
    ];

    return (
        <>
            <div className="Lobby-background vh-100 vw-100 d-flex flex-column position-fixed overflow-y">
                
            </div>
            <PlayerHeader playerAvatar={avatar} player={user} auth={isAuthenticated}></PlayerHeader>
            <div className="container pt-5">
                <div className="mt-5 pt-5 pb-1 position-relative">
                    <h1 className='text-white text-uppercase fw-bold shadow'> Choose Your Opponent</h1>
                </div>
                <ToggleButtonGroup name="category" as={Row} className='gap-2 d-flex flex-md-nowrap align-items-stretch row-cols-5 mx-1 pb-4' defaultValue={"ALL"}>
                    {category.map((category, idx) => (
                        <ToggleButton
                            as={Col}
                            key={category.value}
                            id={`level-${category.value}`}
                            type="radio"
                            variant="outline-warning"
                            size="lg"
                            value={category.name}
                            checked={categoryValue === category.name}
                            onChange={(e) => setCategoryValue(e.currentTarget.value)}
                            className='border-0 fw-bold shadow rounded-3 d-flex justify-content-center align-items-center Primary-bg-color text-white py-sm-3 fs-6 text-wrap text-uppercase'
                        >
                            {category.name}
                        </ToggleButton>
                    ))}
                </ToggleButtonGroup >

                <div> <p>Test</p></div>

                <div className='container row row-cols-lg-4 row-cols-auto overflow-y mvh-50'>
                    {users.map((player, idx) => {
                        if(categoryValue === 'ALL') {
                            return <PlayerCardLobby playerAvatar={avatar} username={player.username} level={player.level} key={idx}/>
                        }else{
                            if(categoryValue === player.level){
                                return <PlayerCardLobby playerAvatar={avatar} username={player.username} level={player.level} key={idx}/>
                            }
                        }
                        return true;
                    })}
                </div>
                <Footer/>
            </div>
            
        </>
    );
}

export default PlayerLobby;
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";

function NotFound(){
    return(
        <div className="vw-100 vh-100 d-flex bg-dark">
            <div className="container text-center my-auto">
                <Card className="bg-primary py-5">
                    <h1 className="badge fs-1"> 404 Not Found!</h1>
                    <Card.Title className="fs-3 pb-3">We're sorry, we couldn't find the page you requested.</Card.Title>
                    <Card.Subtitle className="py-2">Return to home? <Link className="btn btn-dark" to={'/'}>Home</Link></Card.Subtitle>
                </Card>
            </div>
        </div>
    );
}

export default NotFound;
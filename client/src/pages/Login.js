import './assets/css/Auth.css'
import { useState  } from 'react';
import { useNavigate } from 'react-router-dom';
import { Alert, Card, Form, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Authenticated from '../hoc/Authenticated';
import users from './assets/user.json';


function Login(props){
    const [validated, setValidated] = useState(false);
    const [passwordVisible, setPasswordVisible] = useState(false);
    
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [message, setMessage] = useState(null);
    const navigate = useNavigate();
    
    
    function onSubmit(e) {
        e.preventDefault();

        console.log("On submit from Inquiry Page");
        console.log('username: ', username);
        console.log('password: ', password)

        const user = users.find((u) => (u.username === username && u.password === password));

        console.log(user);

        if(user){
            localStorage.setItem("isAuthenticated", JSON.stringify(true));
            localStorage.setItem("user", JSON.stringify({
                username: user.username,
                fullname: user.fullname,
                level: user.level,
                point: user.point
            }))
            // Redirect to home
            navigate("/");
        }
        setMessage("Username or Password is invalid");
        
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            setMessage(null);
            event.preventDefault();
            event.stopPropagation();
        }else{
            setValidated(true); 
            onSubmit(event);
        }
        

    };

    const handlePasswordVisible = () => {
        setPasswordVisible(!passwordVisible);
    }


    return(
        <Authenticated>
        <div className="vw-100 vh-100 Auth-background container-fluid">
            <div className="row justify-content-center my-auto h-100 align-items-center d-flex">
                <div className="col-md-5">
                <Card className="p-4">
                    <Link to={"/"}><i className="bi bi-arrow-left-square-fill fs-1 p-2"></i></Link>
                        <Card.Body className="align-items-center d-flex flex-column">
                            <Card.Title className="fs-1 mb-5 fw-bold">LOGIN</Card.Title>
                            
                            <Form noValidate validated={validated} onSubmit={handleSubmit} className="w-100">
                                {message ? (
                                    <Row className='mb-3'>
                                        <Alert variant="warning">
                                            {message}
                                        </Alert>
                                    </Row>
                                ): (
                                    null
                                )}
                                <Row className="mb-3">
                                    <Form.Group controlId="validationCustom01">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            placeholder="Username"
                                            pattern="^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
                                            onChange={(e) => setUsername(e.target.value)}
                                        />
                                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">Invalid username</Form.Control.Feedback>
                                    </Form.Group>
                                </Row>
                                <Form.Label>Password</Form.Label>
                                <Row className="mb-3">
                                    
                                    <Form.Group as={Col} controlId="validationCustom03" className='col-9'>
                                        
                                        <Form.Control
                                            type={passwordVisible ? "text" : "password"} 
                                            placeholder="Password" 
                                            onChange={(e) => setPassword(e.target.value)}
                                            required pattern='^\S{6,}$'
                                        /> 
                                        
                                        <Form.Control.Feedback type="invalid">
                                            Invalid password
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <i as={Col} className="bi bi-eye fw-bold btn-outline-primary btn col-2 ms-lg-3" onClick={handlePasswordVisible}></i>
                                </Row>
                                    <Button type="submit" variant="warning" className="text-white fw-bold fs-5 w-100" size="sm">Login</Button>
                            </Form>

                            <Card.Subtitle className="my-1 text-muted">
                                Don't have any account? <Link to={"/register"}>Register</Link>
                            </Card.Subtitle>
                            
                        </Card.Body>
                    </Card>
                </div>
                
            </div>
        </div>
        </Authenticated>
    )
}

export default Login;
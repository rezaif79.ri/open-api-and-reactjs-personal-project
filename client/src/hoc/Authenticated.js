import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

const LOCAL_STORAGE_AUTH_STATE_KEY = "isAuthenticated";

const Authenticated = ({ children, redirect = "/" }) => {
  const navigate = useNavigate();

  function onMount() {
    const formState = localStorage.getItem(LOCAL_STORAGE_AUTH_STATE_KEY);
    const isAuthenticated = JSON.parse(formState || "false");

    if (isAuthenticated) navigate(redirect);
  }

  useEffect(onMount);

  return children;
};

export default Authenticated;

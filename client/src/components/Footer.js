import { Col, Row } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

function Footer(){
    return(
        <div className="container-fluid position-relative">
            <div className='d-flex w-100 justify-content-center align-items-center gap-5 mt-5 mb-2 pt-5 pb-2 border-bottom border-white border-3'>
                <NavLink className="text-white text-decoration-none fs-5" to={'/'}>Main</NavLink>
                <NavLink className="text-white text-decoration-none fs-5" to={'/'}>About</NavLink>
                <NavLink className="text-white text-decoration-none fs-5" to={'/'}>Game Features</NavLink>
                <NavLink className="text-white text-decoration-none fs-5" to={'/'}>System Requirement</NavLink>
                <NavLink className="text-white text-decoration-none fs-5" to={'/'}>Quotes</NavLink>
            </div>
            <Row className="row-cols-2 my-3 pb-5">
                <Col className='d-flex justify-content-start align-items-center'>
                    <p className='text-muted'>©2021 Rock Paper Scissor, Inc. All rights reserved</p>
                </Col>
                <Col className='d-flex justify-content-end align-items-center'>
                    <NavLink className="text-white text-decoration-none fs-5 px-3 border-3 border-end" to="/">Privacy Policy</NavLink>
                    <NavLink className="text-white text-decoration-none fs-5 px-3 border-3 border-end" to="/">Privacy Policy</NavLink>
                    <NavLink className="text-white text-decoration-none fs-5 ps-3" to="/">Privacy Policy</NavLink>
                </Col>
            </Row>
        </div>
    );
}

export default Footer; 
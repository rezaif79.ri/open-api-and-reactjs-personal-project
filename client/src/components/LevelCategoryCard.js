import Card  from 'react-bootstrap/Card';

function LevelCategoryCard(props){
    return(
        <>
            <Card id={props.id} className="d-flex justify-content-center Secondary-bg-color col" onClick={props.onCardClicked}>
                <Card.Body className="card-body d-flex align-items-center justify-content-center">
                    <Card.Title className="text-white text-center text-uppercase fs-6 fw-bold">{props.title}</Card.Title>
                </Card.Body>
            </Card>
        </>
    )
}

export default LevelCategoryCard;
import { Dropdown, Navbar, Container, Nav, Row } from "react-bootstrap";
import { NavLink } from 'react-router-dom';
import './PlayerHeader.css';

function PlayerHeader(props){
    function onLogoutClick(){
        localStorage.removeItem('isAuthenticated');
        localStorage.removeItem('user');
    }

    return(
        <Navbar /* collapseOnSelect expand="md"  */bg="dark" variant="dark" className="shadow vw-100 position-fixed top-index">
            <Container>
                <NavLink className="navbar-brand" to={"/"}>Rock Paper Scissor</NavLink>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    {/* {props.isAuthenticated */ props.auth ? (
                        <Dropdown id="offcanvasNavbarDropdown" className="me-1 ms-auto min-width-200">
                            <Dropdown.Toggle id="dropdown-button-user" variant="secondary">
                                <i className="bi bi-person-circle">{' '}</i>{props.player.username}
                            </Dropdown.Toggle>

                            <Dropdown.Menu variant="dark" className="Primary-bg-color container">
                                <Row className="gap-2">
                                    <div className="col-4 d-flex justify-content-end pe-0 ms-2 mt-1">
                                        <img className="User-ava-img" src={props.playerAvatar} alt="Ava"/>
                                    </div>
                                    <div className="col-7 mt-2">
                                        <h5 className="fw-bold Text-user-title"> {props.player.fullname}</h5>
                                        <h6 className="fw-normal">  {props.player.level} </h6>
                                        
                                    </div>
                                </Row>
                                <Row className="text-center">
                                    <p className="text-light fw-light my-2"> {props.player.point} Points</p>
                                    <NavLink to={"/login"} onClick={onLogoutClick} className="btn btn-outline-warning w-50 mx-auto">Logout</NavLink>
                                </Row>
                            </Dropdown.Menu>
                        </Dropdown>
                    ) :(
                        <Nav className="ms-auto me-0 gap-3 nav-item">
                            <NavLink className="nav-link fs-6 text-light" to={"/login"}><i className="bi bi-fingerprint fs-5">{' '}</i>Login</NavLink>
                            <NavLink className="nav-link fs-6 text-light" to={"/register"}><i className="bi bi-menu-button fs-5">{' '}</i>Register</NavLink>
                        </Nav>
                        
                    )}
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default PlayerHeader;
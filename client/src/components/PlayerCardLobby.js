import { Card, Col, Row } from 'react-bootstrap';

function PlayerCardLobby(props){
    return(
        <Col>
            <Card className='container-fluid m-2 p-3 rounded-3 bg-dark'>
                <Row className='pt-2 pb-3'>
                    <Col className="col-4 d-flex justify-content-center pe-0">
                        <img className="User-ava-img" src={props.playerAvatar} alt="Ava"/>
                    </Col>
                    <Col className='col-auto mt-2'>
                        <Card.Title className='text-white'>{props.username}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">{props.level}</Card.Subtitle>
                    </Col>
                </Row>
                
                <Card.Text className='text-white'>
                    Never Should Have Come Here
                </Card.Text>
                <Card.Subtitle className='text-muted'>Since November 22, 2021</Card.Subtitle>
                <Card.Link className='btn btn-warning mt-3 text-white fw-bold' href="#">Fight</Card.Link>
            </Card>
        </Col>
    );
}

export default PlayerCardLobby;
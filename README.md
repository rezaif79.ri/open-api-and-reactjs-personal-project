# Title
Open Api Docs, Mock Server and React App

# Description
- Create Open Api docs with swagger for Rock Paper Scissor API.
- Generate mock server from swagger editor and install it in server folder.
- Create React app for Rock Paper Scissor (Login, Register, and Player Lobby page).

# Installation
- Clone this repo with with ssh or https:

```shell
git clone git@gitlab.com:rezaif79.ri/open-api-and-reactjs-personal-project.git
```

```shell
git clone https://gitlab.com/rezaif79.ri/open-api-and-reactjs-personal-project.git
```

- To install React app, Go to client folder and run shell 'npm install' then 'npm start' 
- To install Mock server, Go to server folder and run shell 'npm run prestart' then 'npm start'

# Usage
React App is a Front end single page app for client, To do login, register and challenge other player in player lobby, After starting the react server, goto localhost:3000 (default).

Mock server app is an api mock server built with swagger, After run the server goto localhost:8080/docs to see the Api documentation, to test the api you can use tools like postman or thunderclient and hit localhost:8080/v1/.....

# Build With
- Node.js
- ReactJs
- Swagger Editor

# Author
**Reza Izzan Fadhila**

- [Profile](https://github.com/rezaif79-ri)
- [Email](rezaif79.ri@gmail.com)
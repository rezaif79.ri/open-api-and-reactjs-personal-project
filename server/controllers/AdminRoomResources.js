'use strict';

var utils = require('../utils/writer.js');
var AdminRoomResources = require('../service/AdminRoomResourcesService');

module.exports.adminV1RoomsGET = function adminV1RoomsGET (req, res, next) {
  AdminRoomResources.adminV1RoomsGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.adminV1RoomsIdGET = function adminV1RoomsIdGET (req, res, next, id) {
  AdminRoomResources.adminV1RoomsIdGET(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

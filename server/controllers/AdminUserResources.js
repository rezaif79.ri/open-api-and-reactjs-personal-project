'use strict';

var utils = require('../utils/writer.js');
var AdminUserResources = require('../service/AdminUserResourcesService');

module.exports.adminV1UsersIdGET = function adminV1UsersIdGET (req, res, next, id) {
  AdminUserResources.adminV1UsersIdGET(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllUsers = function getAllUsers (req, res, next) {
  AdminUserResources.getAllUsers()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

'use strict';

var utils = require('../utils/writer.js');
var PlayerProfileResources = require('../service/PlayerProfileResourcesService');

module.exports.v1ProfilesBiodataGET = function v1ProfilesBiodataGET (req, res, next, xAccessToken) {
  PlayerProfileResources.v1ProfilesBiodataGET(xAccessToken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1ProfilesBiodataPOST = function v1ProfilesBiodataPOST (req, res, next, body, xAccessToken) {
  PlayerProfileResources.v1ProfilesBiodataPOST(body, xAccessToken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1ProfilesBiodataPUT = function v1ProfilesBiodataPUT (req, res, next, body, xAccessToken) {
  PlayerProfileResources.v1ProfilesBiodataPUT(body, xAccessToken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1ProfilesGET = function v1ProfilesGET (req, res, next, xAccessToken) {
  PlayerProfileResources.v1ProfilesGET(xAccessToken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1ProfilesStatsGET = function v1ProfilesStatsGET (req, res, next, xAccessToken) {
  PlayerProfileResources.v1ProfilesStatsGET(xAccessToken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

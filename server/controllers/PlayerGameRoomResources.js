'use strict';

var utils = require('../utils/writer.js');
var PlayerGameRoomResources = require('../service/PlayerGameRoomResourcesService');

module.exports.v1RoomsChallengeAcceptRoomIdPOST = function v1RoomsChallengeAcceptRoomIdPOST (req, res, next, xAccessToken, roomId) {
  PlayerGameRoomResources.v1RoomsChallengeAcceptRoomIdPOST(xAccessToken, roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsChallengeGET = function v1RoomsChallengeGET (req, res, next, xAccessToken) {
  PlayerGameRoomResources.v1RoomsChallengeGET(xAccessToken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsChallengePlayerIdPOST = function v1RoomsChallengePlayerIdPOST (req, res, next, xAccessToken, playerId) {
  PlayerGameRoomResources.v1RoomsChallengePlayerIdPOST(xAccessToken, playerId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsChallengeRejectRoomIdPOST = function v1RoomsChallengeRejectRoomIdPOST (req, res, next, xAccessToken, roomId) {
  PlayerGameRoomResources.v1RoomsChallengeRejectRoomIdPOST(xAccessToken, roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsCreatePOST = function v1RoomsCreatePOST (req, res, next, xAccessToken) {
  PlayerGameRoomResources.v1RoomsCreatePOST(xAccessToken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsGET = function v1RoomsGET (req, res, next, xAccessToken) {
  PlayerGameRoomResources.v1RoomsGET(xAccessToken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsJoinRoomIdPOST = function v1RoomsJoinRoomIdPOST (req, res, next, xAccessToken, roomId) {
  PlayerGameRoomResources.v1RoomsJoinRoomIdPOST(xAccessToken, roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsMy_gamesGET = function v1RoomsMy_gamesGET (req, res, next, xAccessToken, state) {
  PlayerGameRoomResources.v1RoomsMy_gamesGET(xAccessToken, state)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsMy_gamesRoundsGET = function v1RoomsMy_gamesRoundsGET (req, res, next, xAccessToken, status) {
  PlayerGameRoomResources.v1RoomsMy_gamesRoundsGET(xAccessToken, status)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsRoomIdGET = function v1RoomsRoomIdGET (req, res, next, xAccessToken, roomId) {
  PlayerGameRoomResources.v1RoomsRoomIdGET(xAccessToken, roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsRoomIdPlayOptionPOST = function v1RoomsRoomIdPlayOptionPOST (req, res, next, xAccessToken, roomId, option) {
  PlayerGameRoomResources.v1RoomsRoomIdPlayOptionPOST(xAccessToken, roomId, option)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

'use strict';

var utils = require('../utils/writer.js');
var PlayerUserResources = require('../service/PlayerUserResourcesService');

module.exports.v1UserSearchGET = function v1UserSearchGET (req, res, next, id, level, minPoint, maxPoint) {
  PlayerUserResources.v1UserSearchGET(id, level, minPoint, maxPoint)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

'use strict';

var utils = require('../utils/writer.js');
var Other = require('../service/OtherService');

module.exports.getRoot = function getRoot (req, res, next) {
  Other.getRoot()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

'use strict';

var utils = require('../utils/writer.js');
var AdminRoundResources = require('../service/AdminRoundResourcesService');

module.exports.adminV1RoundsIdGET = function adminV1RoundsIdGET (req, res, next, id) {
  AdminRoundResources.adminV1RoundsIdGET(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

'use strict';


/**
 * Used to retrieve game round data with specific room id
 *  Use this to get specific game round data, Only finished games that will be shown It will return games data in list
 *
 * id UUID Game round id
 * returns inline_response_200_6
 **/
exports.adminV1RoundsIdGET = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "id" : "a29a915c-892a-40b9-a91e-ccc69ef04ccf",
    "roomId" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "status" : "FINISHED",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


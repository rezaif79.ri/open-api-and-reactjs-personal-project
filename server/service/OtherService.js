'use strict';


/**
 * Used to check server status
 * Hit or ping this endpoint to check the server status.
 *
 * returns inline_response_200
 **/
exports.getRoot = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Request was successful"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


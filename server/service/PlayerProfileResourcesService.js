'use strict';


/**
 * Used to get user owned biodata
 * Use this to get biodata with JWT in headers
 *
 * xAccessToken String The user to create.
 * returns inline_response_200_7
 **/
exports.v1ProfilesBiodataGET = function(xAccessToken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "phoneNumber" : "0212312423423",
    "address" : "Jln. Bambu Kuning",
    "avatarUrl" : "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
    "bio" : "I'm a passionate gamers",
    "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "userId" : "e8c80105-7aed-4c7c-9811-b4fd5a869660",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to create user biodata
 * Use this to create user biodata if not exist with request body and JWT in headers
 *
 * body Object All field are optional to given in request body (optional)
 * xAccessToken String The user to create.
 * returns inline_response_201_2
 **/
exports.v1ProfilesBiodataPOST = function(body,xAccessToken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Created"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to update user biodata
 * Use this to update biodata with request body and JWT in headers
 *
 * body Object All field are optional to given in request body (optional)
 * xAccessToken String The user to create.
 * returns inline_response_201_1
 **/
exports.v1ProfilesBiodataPUT = function(body,xAccessToken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Updated"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to get user profile
 * Use this to get profile with JWT in headers
 *
 * xAccessToken String The user to create.
 * returns inline_response_200_3
 **/
exports.v1ProfilesGET = function(xAccessToken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "biodata" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "phoneNumber" : "0212312423423",
      "address" : "Jln. Bambu Kuning",
      "avatarUrl" : "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
      "bio" : "I'm a passionate gamers",
      "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "userId" : "e8c80105-7aed-4c7c-9811-b4fd5a869660",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "user" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to get user game statistics
 * Use this to get user game statistics with JWT in headers
 *
 * xAccessToken String The user to create.
 * returns inline_response_200_8
 **/
exports.v1ProfilesStatsGET = function(xAccessToken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "winCount" : 10,
    "loseCount" : 2,
    "level" : {
      "minPoint" : 0,
      "maxPoint" : 1199,
      "name" : "Novice",
      "id" : 1
    },
    "id" : "a0b4c049-bc15-4e94-98a0-dcb861b93aa2",
    "point" : 1000
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


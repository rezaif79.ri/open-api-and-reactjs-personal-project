'use strict';


/**
 * Used for Player to accept challenge from other Player
 *  Use this for Player to accept the challenge from other Player It will return the challenge that accepted by this Player 
 *
 * xAccessToken String The user to create.
 * roomId UUID UUID of the game room
 * returns inline_response_200_12
 **/
exports.v1RoomsChallengeAcceptRoomIdPOST = function(xAccessToken,roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "room" : {
      "roundCount" : 3,
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "winner" : { },
      "name" : "Suit Game 1",
      "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
      "state" : "OPENED",
      "roundId" : [ null, null, null ],
      "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",null]",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used for Player to see challenge from other Player
 *  Use this for Player to see the challenge from other Player It will return the challenge that pointed to this Player 
 *
 * xAccessToken String The user to create.
 * returns inline_response_200_11
 **/
exports.v1RoomsChallengeGET = function(xAccessToken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : { },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "CLOSED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", null, null ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"ffe47c73-4424-44d6-ba56-89e83ba9092f\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  }, {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : { },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "CLOSED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", null, null ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"ffe47c73-4424-44d6-ba56-89e83ba9092f\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used for Player to challenge other Player
 *  Use this for Player to challenge other Player, Only accepted challenge will be created game room and rounds. If the challenge rejected game room will be closed and abandoned. Player JWT required in header. It will return room data.
 *
 * xAccessToken String The user to create.
 * playerId UUID UUID of the player
 * returns inline_response_201_4
 **/
exports.v1RoomsChallengePlayerIdPOST = function(xAccessToken,playerId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Waiting for your opponent to accept.",
    "room" : {
      "roundCount" : 3,
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "winner" : { },
      "name" : "Suit Game 1",
      "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
      "state" : "OPENED",
      "roundId" : [ null, null, null ],
      "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",null]",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used for Player to reject challenge from other Player
 *  Use this for Player to reject the challenge from other Player It will return the challenge that rejected by this Player 
 *
 * xAccessToken String The user to create.
 * roomId UUID UUID of the game room
 * returns inline_response_201_3
 **/
exports.v1RoomsChallengeRejectRoomIdPOST = function(xAccessToken,roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Game"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used for Player to join game room
 *  Use this for Player to join game room. Player JWT required in header. It will return room data.
 *
 * xAccessToken String The user to create.
 * returns inline_response_201_5
 **/
exports.v1RoomsCreatePOST = function(xAccessToken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Send roomId to your opponen or wait for someone to join your room.",
    "room" : {
      "roundCount" : 3,
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "winner" : { },
      "name" : "Suit Game 1",
      "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
      "state" : "CLOSED",
      "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", null, null ],
      "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"ffe47c73-4424-44d6-ba56-89e83ba9092f\"]",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to retrieve all opened game room data in list
 *  Use this to get all games data, Only opened games that will be shown Player JWT required in header. It will return games data in list
 *
 * xAccessToken String The user to create.
 * returns inline_response_200_4
 **/
exports.v1RoomsGET = function(xAccessToken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  }, {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used for Player to join game room
 *  Use this for Player to join game room. Player JWT required in header. It will return room data.
 *
 * xAccessToken String The user to create.
 * roomId UUID UUID of the game room
 * returns inline_response_201_6
 **/
exports.v1RoomsJoinRoomIdPOST = function(xAccessToken,roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Redirect to /v1/rooms/{roomId}/play to play game rounds.",
    "room" : {
      "roundCount" : 3,
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "winner" : { },
      "name" : "Suit Game 1",
      "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
      "state" : "CLOSED",
      "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", null, null ],
      "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"ffe47c73-4424-44d6-ba56-89e83ba9092f\"]",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to retrieve all game room data played by a Player in list
 *  Use this to get all games data, Only games that a Player are in will be shown  Player can filter the game room by OPENED or CLOSED option in query '?state'  Player JWT required in header. It will return games data in list 
 *
 * xAccessToken String The user to create.
 * state state Filter by state
 * returns inline_response_200_4
 **/
exports.v1RoomsMy_gamesGET = function(xAccessToken,state) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  }, {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to retrieve all game round data played by a Player in list
 *  Use this to get all game rounddata, Only games that a Player are in will be shown Player round can be filtered with query '?status' which ACTIVE or FINISHED Player JWT required in header. It will return game round data in list
 *
 * xAccessToken String The user to create.
 * status status Filter by status
 * returns inline_response_200_10
 **/
exports.v1RoomsMy_gamesRoundsGET = function(xAccessToken,status) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "round" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "winner" : {
        "createdAt" : "2021-07-30 17:00:00 +0700",
        "role" : {
          "name" : "Player",
          "id" : 1
        },
        "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
        "username" : "robert79",
        "updatedAt" : "2021-07-30 17:00:00 +0700"
      },
      "id" : "a29a915c-892a-40b9-a91e-ccc69ef04ccf",
      "roomId" : "3d5594be-f155-46a9-9746-725e45d1e794",
      "status" : "FINISHED",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "gameOption" : {
      "inferiodId" : 2,
      "name" : "ROCK",
      "id" : 1,
      "superiorId" : 3
    },
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  }, {
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "round" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "winner" : {
        "createdAt" : "2021-07-30 17:00:00 +0700",
        "role" : {
          "name" : "Player",
          "id" : 1
        },
        "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
        "username" : "robert79",
        "updatedAt" : "2021-07-30 17:00:00 +0700"
      },
      "id" : "a29a915c-892a-40b9-a91e-ccc69ef04ccf",
      "roomId" : "3d5594be-f155-46a9-9746-725e45d1e794",
      "status" : "FINISHED",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "gameOption" : {
      "inferiodId" : 2,
      "name" : "ROCK",
      "id" : 1,
      "superiorId" : 3
    },
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to retrieve specific game room data of a Player
 *  Use this to get specific game room data, Only game room that player are in will be shown Player JWT required in header. It will return games data in list
 *
 * xAccessToken String The user to create.
 * roomId UUID UUID of the game room
 * returns inline_response_200_4
 **/
exports.v1RoomsRoomIdGET = function(xAccessToken,roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  }, {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used for Player to play specific game and pick option
 *  Use this for Player to play game round in game room with sending the option in path Player JWT required in header. It will return game round data
 *
 * xAccessToken String The user to create.
 * roomId UUID UUID of the game room
 * option String The option of Rock, Paper or Scissors
 * returns inline_response_200_9
 **/
exports.v1RoomsRoomIdPlayOptionPOST = function(xAccessToken,roomId,option) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "round" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "round" : {
        "createdAt" : "2021-07-30 17:00:00 +0700",
        "winner" : {
          "createdAt" : "2021-07-30 17:00:00 +0700",
          "role" : {
            "name" : "Player",
            "id" : 1
          },
          "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
          "username" : "robert79",
          "updatedAt" : "2021-07-30 17:00:00 +0700"
        },
        "id" : "a29a915c-892a-40b9-a91e-ccc69ef04ccf",
        "roomId" : "3d5594be-f155-46a9-9746-725e45d1e794",
        "status" : "FINISHED",
        "updatedAt" : "2021-07-30 17:00:00 +0700"
      },
      "gameOption" : {
        "inferiodId" : 2,
        "name" : "ROCK",
        "id" : 1,
        "superiorId" : 3
      },
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


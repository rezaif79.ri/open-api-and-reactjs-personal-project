'use strict';


/**
 * Used to retrieve all finished game room data in list
 *  Use this to get all games data, Only finished games that will be shown It will return games data in list
 *
 * returns inline_response_200_4
 **/
exports.adminV1RoomsGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  }, {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to retrieve game room data with specific room id
 *  Use this to get specific game room data, Only finished game room that will be shown  It will return details of game room data in object
 *
 * id UUID Game rooms id
 * returns inline_response_200_5
 **/
exports.adminV1RoomsIdGET = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 3,
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "winner" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "name" : "Suit Game 1",
    "id" : "3d5594be-f155-46a9-9746-725e45d1e794",
    "state" : "OPENED",
    "roundId" : [ "b352fcbd-9d3f-4994-8cd1-7c701cc341c1", "419bb676-315c-43fe-bf0f-a9ee4ce6391b", "22c400ab-a663-4e8c-9d83-5afa6ba69621" ],
    "playerId" : "[\"bdaf6814-2e99-40d5-9c15-e0238a90c886\",\"f7632955-e6b2-48ac-9e7f-2bf7ea5b235b\"]",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


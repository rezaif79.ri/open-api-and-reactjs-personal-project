'use strict';


/**
 * Used to retrieve user data with specific identity
 * Use this to get user data with id in request params or by level or by points If not giving specific identity, will return all Player with not filtering.
 *
 * id String Search by Player UUID  *Optional (optional)
 * level BigDecimal Search by Player level  *Optional (optional)
 * minPoint BigDecimal Search by minimum point Player  *Optional (optional)
 * maxPoint BigDecimal Search by minimum point Player  *Optional (optional)
 * returns inline_response_200_13
 **/
exports.v1UserSearchGET = function(id,level,minPoint,maxPoint) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "level" : {
      "minPoint" : 0,
      "maxPoint" : 1199,
      "name" : "Novice",
      "id" : 1
    },
    "biodata" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "phoneNumber" : "0212312423423",
      "address" : "Jln. Bambu Kuning",
      "avatarUrl" : "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
      "bio" : "I'm a passionate gamers",
      "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "userId" : "e8c80105-7aed-4c7c-9811-b4fd5a869660",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "user" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  }, {
    "level" : {
      "minPoint" : 0,
      "maxPoint" : 1199,
      "name" : "Novice",
      "id" : 1
    },
    "biodata" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "phoneNumber" : "0212312423423",
      "address" : "Jln. Bambu Kuning",
      "avatarUrl" : "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
      "bio" : "I'm a passionate gamers",
      "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "userId" : "e8c80105-7aed-4c7c-9811-b4fd5a869660",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "user" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


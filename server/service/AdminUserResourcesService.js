'use strict';


/**
 * Used to retrieve user data with specific user id
 * Use this to get user data with id in request params
 *
 * id String The user UUID V4
 * returns inline_response_200_3
 **/
exports.adminV1UsersIdGET = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "biodata" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "phoneNumber" : "0212312423423",
      "address" : "Jln. Bambu Kuning",
      "avatarUrl" : "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
      "bio" : "I'm a passionate gamers",
      "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "userId" : "e8c80105-7aed-4c7c-9811-b4fd5a869660",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    },
    "user" : {
      "createdAt" : "2021-07-30 17:00:00 +0700",
      "role" : {
        "name" : "Player",
        "id" : 1
      },
      "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
      "username" : "robert79",
      "updatedAt" : "2021-07-30 17:00:00 +0700"
    }
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Used to retrieve all user data in list
 *  Returns a list of user. The API returns a 200 OK. Also, this API returns the list of all user 
 *
 * returns inline_response_200_2
 **/
exports.getAllUsers = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "role" : {
      "name" : "Player",
      "id" : 1
    },
    "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
    "username" : "robert79",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  }, {
    "createdAt" : "2021-07-30 17:00:00 +0700",
    "role" : {
      "name" : "Player",
      "id" : 1
    },
    "id" : "bdaf6814-2e99-40d5-9c15-e0238a90c886",
    "username" : "robert79",
    "updatedAt" : "2021-07-30 17:00:00 +0700"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

